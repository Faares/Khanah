@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <ol class="breadcrumb" style="margin-top:0px;">
              <li><a href="{{ url('/cp') }}">خَانة</a></li>
              <li><a href='{{ url('/cp/projects') }}'>المشاريع</a></li>
              <li class="active">{{$project->name}}</li>
            </ol>
            @if(Session::has('success'))
            <p class='alert alert-success'>{{Session::get('success')}}</p>
            @endif
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">{{$project->name}}</div>
                    <table class="table table-bordered">
                        <tr>
                            <td>#</td>
                            <td>الاسم</td>
                            <td>التفاصيل</td>
                            <td>الجهة</td>
                            <td>أُنشئ بتاريخ</td>
                            <td>آخر تحديث</td>
                        </tr>
                        <tr>
                            <td>{{ $project->id }}</td>
                            <td>{{$project->name}}</td>
                            <td>{{$project->details}}</td>
                            <td>{{$project->payee}}</td>
                            <td dir="ltr">{{$project->created_at}}</td>
                            <td dir="ltr">{{$project->updated_at}}</td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-success">
                    <div class="panel-heading">
                        الإيرادات
                    </div>
                    <table class="table table-bordered">
                        <tr>
                            <td>تاريخ الاستحقاق</td>
                            <td>تاريخ الاستلام</td>
                            <td>المبلغ المستحق</td>
                            <td>المبلغ المستلم</td>
                            <td>نسبة الاستلام</td>
                            <td>التفاصيل</td>
                            <td>صورة الفاتورة</td>
                        </tr>
                        @if($project->incomes)
                        @foreach($project->incomes as $income)
                        <tr>
                            <td dir="ltr">{{$income->due_date}}</td>
                            <td dir="ltr">{{$income->received_date}}</td>
                            <td>{{$income->amount_due}}</td>
                            <td>{{$income->amount_received}}</td>
                            <td>{{($income->amount_received / $income->amount_due) * 100}}%</td>
                            <td>{{$income->details}}</td>
                            @if(isset($income->invoice) && !empty($income->invoice))
                            <td><a href="{{url("cp/invoices/income/$income->id")}}" target='_blank'>الفاتورة</a></td>
                            @else
                            <td>-</td>
                            @endif
                        </tr>
                        @endforeach
                        @else
                        <tr>
                            <td colspan='7'>لا توجد إيرادات بعد.</td>
                        </tr>
                        @endif
                    </table>
                </div>
            </div>
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-danger">
                    <div class="panel-heading">
                        المصروفات
                    </div>
                    <table class="table table-bordered">
                        <tr>
                            <td>الحدث</td>
                            <td>التفاصيل</td>
                            <td>الجهة</td>
                            <td>التاريخ</td>
                            <td>مستحقة الدفع</td>
                            <td>التكلفة</td>
                            <td>المبلغ المدفوع</td>
                            <td>الفاتورة</td>
                        </tr>
                        @if($project->expenses)
                        @foreach($project->expenses as $expense)
                        <tr>
                            <td>{{$expense->action}}</td>
                            <td>{{$expense->details}}</td>
                            <td>{{$expense->payee}}</td>
                            <td dir="ltr">{{$expense->date}}</td>
                            <td>{{$expense->due_payment ? "نعم" : "لا"}}</td>
                            <td>{{$expense->cost}}</td>
                            <td>{{$expense->amount_paid}}</td>
                            @if(isset($expense->invoice) && !empty($expense->invoice))
                            <td><a href="{{url("cp/invoices/expense/$expense->id")}}" target='_blank'>الفاتورة</a></td>
                            @else
                            <td>-</td>
                            @endif
                        </tr>
                        @endforeach
                        @else
                            Error
                        @endif
                    </table>
                </div>
            </div>
            <div class="col-md-8 col-md-offset-2">
                <a href="{{url("/cp/projects/$project->id/edit")}}" class="btn btn-primary">تعديل</a>
            </div>
        </div>
    </div>
@endsection
