@extends('layouts.app')

@section('scripts')
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
@endsection

@section('stylesheet')
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@endsection


@section('content')
    <div class="container">
        <div class="row">
            <ol class="breadcrumb" style="margin-top:0px;">
              <li><a href="{{ url('/cp') }}">خَانة</a></li>
              <li><a href='{{ url('/cp/projects') }}'>المشاريع</a></li>
              <li class="active">إنشاء مشروع</li>
            </ol>
            @if($errors)
            <ul class="list-group">
                @foreach($errors->all() as $error)
                <li class="list-group-item">{{$error}}</li>
                @endforeach
            </ul>
            @endif
            {!! Form::open(['route'=>['projects.store'],'method'=>'POST','files'=>true]) !!}
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">تفاصيل المشروع</div>
                    <table class="table table-bordered">
                        <tr>
                            <td>الاسم</td>
                            <td>التفاصيل</td>
                            <td>الجهة</td>
                        </tr>
                        <tr>
                            <td>{!! Form::text('project[name]',null,['class'=>'form-control']) !!}</td>
                            <td>{!! Form::textarea('project[details]',null,['class'=>'form-control']) !!}</td>
                            <td>{!! Form::text('project[payee]',null,['class'=>'form-control']) !!}</td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-success">
                    <div class="panel-heading">
                        الإيرادات
                    </div>
                    <table class="table table-bordered" style="direction:rtl" id='incomesTable'>
                        <tr>
                            <td>تاريخ الاستحقاق</td>
                            <td>تاريخ الاستلام</td>
                            <td>المبلغ المستحق</td>
                            <td>المبلغ المستلم</td>
                            <td>التفاصيل</td>
                            <td>صورة الفاتورة</td>
                        </tr>
                        <tr>
                            <td colspan='7'><button class="btn btn-primary" id='addIncome' data-i="">أضف إيراد</button></td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-danger">
                    <div class="panel-heading">
                        المصروفات
                    </div>
                    <table class="table table-bordered" id='expenseTable'>
                        <tr>
                            <td>الحدث</td>
                            <td>التفاصيل</td>
                            <td>الجهة</td>
                            <td>التاريخ</td>
                            <td>مستحقة الدفع</td>
                            <td>التكلفة</td>
                            <td>المبلغ المدفوع</td>
                            <td>الفاتورة</td>
                        </tr>
                        <!--<tr>
                            <td>{!! Form::text("expenses[0][action]",null,['class'=>'form-control']) !!}</td>
                            <td>{!! Form::textarea("expenses[0][details]",null,['class'=>'form-control']) !!}</td>
                            <td>{!! Form::text("expenses[0][payee]",null,['class'=>'form-control']) !!}</td>
                            <td dir="ltr">{!! Form::date("expenses[0][date]",null,['class'=>'form-control']) !!}</td>
                            <td>{!! Form::checkbox("expenses[0][due_payment]",null,true)  !!}</td>
                            <td>{!! Form::number("expenses[0][cost]",null,['class'=>'form-control']) !!}</td>
                            <td>{!! Form::number("expenses[0][amount_paid]",null,['class'=>'form-control']) !!}</td>
                            <td>{!! Form::file("expenses[0][invoice]",null,['class'=>'form-control']) !!}</td>
                        </tr>-->
                        <tr>
                            <td colspan='7'><button class="btn btn-primary" id='addExpense' data-i="">أضف مصروف</button></td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="col-md-8 col-md-offset-2">
                {!! Form::submit('إنشاء',['class'=>'btn btn-primary']) !!}
            </div>
             {!! Form::close() !!}
        </div>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){

            incomesI = $("#addIncome").data('i');
            $("#addIncome").click(function(){

                incomesI = incomesI + 1;

                $("#incomesTable tr:last").prev().after(
                    "<tr><td><input type='date' class='form-control datepicker' name='incomes["+incomesI+"][due_date]'/></td>\
                    <td><input type='date' class='form-control datepicker' name='incomes["+incomesI+"][received_date]'/></td>\
                    <td><input type='number' id='amount_due' class='form-control' name='incomes["+incomesI+"][amount_due]'/></td>\
                    <td><input type='number' id='amount_received' class='form-control' name='incomes["+incomesI+"][amount_received]'/></td>\
                    <td><textarea class='form-control' name='incomes["+incomesI+"][details]'/></td>\
                    <td><input name='incomes["+incomesI+"][invoice]' type='file'></td></tr>"
                );

                return false;
            });
            expenseI = $("#addExpense").data('i');
            $("#addExpense").click(function(){
                expenseI = expenseI + 1;
                $("#expenseTable tr:last").prev().after(
                    "<tr><td><input type='text' class='form-control' name='expenses["+expenseI+"][action]'/></td>\
                    <td><textarea class='form-control' name='expenses["+expenseI+"][details]'></textarea></td>\
                    <td><input type='text' class='form-control' name='expenses["+expenseI+"][payee]'/></td>\
                    <td><input type='date' class='form-control datepicker' name='expenses["+expenseI+"][date]'</td>\
                    <td><input type='checkbox' name='expenses["+expenseI+"][due_payment]'/></td>\
                    <td><input type='number' class='form-control' name='expenses["+expenseI+"][cost]'/></td>\
                    <td><input type='number' class='form-control' name='expenses["+expenseI+"][amount_paid]'/></td>\
                    <td><input name='expenses["+expenseI+"][invoice]' type='file'></td></tr>"
                )
                return false;
            });
            $('body').on('focus',".datepicker", function(){
                $(this).datepicker({
                    dateFormat: "yy-mm-dd"
                });
            });
        });
    </script>
@endsection
