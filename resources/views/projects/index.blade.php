@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <ol class="breadcrumb" style="margin-top:0px;">
              <li><a href="{{ url('/cp') }}">خَانة</a></li>
              <li class="active">المشاريع</li>
            </ol>
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <span class="pull-right">المشاريع</span>
                        <span class="pull-left"><a href='{{url('/cp/projects/create')}}' class='btn btn-primary'>إضافة مشروع</a></span>
                        <div class='clearfix'></div>
                    </div>
                    <table class="table table-striped table-bordered ">
                        <tr>
                            <td>#</td>
                            <td>الاسم</td>
                            <td>التفاصيل</td>
                            <td>الجهة</td>
                            <td>أُنشئ بتاريخ</td>
                            <td>آخر تحديث</td>
                        </tr>
                        @foreach ($projects as $project)
                        <tr>
                            <td>{{ $project->id }}</td>
                            <td><a href="{{url("cp/projects/$project->id")}}">{{$project->name}}</a></td>
                            <td>{{$project->details}}</td>
                            <td>{{$project->payee}}</td>
                            <td dir="ltr">{{$project->created_at}}</td>
                            <td dir="ltr">{{$project->updated_at}}</td>
                        </tr>
                        @endforeach
                        <tr style="text-align:center">
                            <td colspan='6'>{{$projects->links()}}</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
