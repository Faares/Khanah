@extends('layouts.app')

@section('scripts')
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
@endsection

@section('stylesheet')
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <ol class="breadcrumb" style="margin-top:0px;">
              <li><a href="{{ url('/cp') }}">خَانة</a></li>
              <li><a href='{{ url('/cp/projects') }}'>المشاريع</a></li>
              <li class="active">تعديل مشروع</li>
            </ol>
            @if($errors)
            <ul class="list-group">
                @foreach($errors->all() as $error)
                <li class="list-group-item">{{$error}}</li>
                @endforeach
            </ul>
            @endif
            {!! Form::open(['route'=>['projects.update',$project->id],'method'=>'PUT']) !!}
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">{{$project->name}}</div>
                    <table class="table table-bordered">
                        <tr>
                            <td>#</td>
                            <td>الاسم</td>
                            <td>التفاصيل</td>
                            <td>الجهة</td>
                            <td>أُنشئ بتاريخ</td>
                            <td>آخر تحديث</td>
                        </tr>
                        <tr>
                            <td>{{ $project->id }}</td>
                            <td>{!! Form::text('project[name]',$project->name,['class'=>'form-control']) !!}</td>
                            <td>{!! Form::textarea('project[details]',$project->details,['class'=>'form-control']) !!}</td>
                            <td>{!! Form::text('project[payee]',$project->payee,['class'=>'form-control']) !!}</td>
                            <td dir="ltr">{{$project->created_at}}</td>
                            <td dir="ltr">{{$project->updated_at}}</td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-success">
                    <div class="panel-heading">
                        الإيرادات
                    </div>
                    <table class="table table-bordered" style="direction:rtl" id='incomesTable'>
                        <tr>
                            <td>تاريخ الاستحقاق</td>
                            <td>تاريخ الاستلام</td>
                            <td>المبلغ المستحق</td>
                            <td>المبلغ المستلم</td>
                            <td>التفاصيل</td>
                            <td>صورة الفاتورة</td>
                        </tr>
                        @if($project->incomes)
                        @php ($i = 0)
                        @foreach($project->incomes as $income)
                        <tr>
                            <td dir="ltr">{!! Form::date("incomes[$i][due_date]",$income->due_date,['class'=>'form-control datepicker']) !!}</td>
                            <td dir="ltr">{!! Form::date("incomes[$i][received_date]",$income->received_date,['class'=>'form-control datepicker']) !!}</td>
                            <td>{!! Form::number("incomes[$i][amount_due]",$income->amount_due,['class'=>'form-control']) !!}</td>
                            <td>{!! Form::number("incomes[$i][amount_received]",$income->amount_received,['class'=>'form-control']) !!}</td>
                            <td>{!! Form::textarea("incomes[$i][details]",$income->details,['class'=>'form-control']) !!}</td>
                            <td><a href="{{url("/cp/invoices/income/$income->id")}}" target="_blank">الفاتورة</a>
                            {!! Form::file("incomes[$i]['invoice']") !!}</td>
                            {!! Form::hidden("incomes[$i][id]",$income->id) !!}
                        </tr>
                        @php ($i = $i + 1)
                        @endforeach
                        @endif
                        <tr>
                            <td colspan='7'><button class="btn btn-primary" id='addIncome' data-i="{{$i-1}}">أضف إيراد</button></td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-danger">
                    <div class="panel-heading">
                        المصروفات
                    </div>
                    <table class="table table-bordered" id='expenseTable'>
                        <tr>
                            <td>الحدث</td>
                            <td>التفاصيل</td>
                            <td>الجهة</td>
                            <td>التاريخ</td>
                            <td>مستحقة الدفع</td>
                            <td>التكلفة</td>
                            <td>المبلغ المدفوع</td>
                            <td>الفاتورة</td>
                        </tr>
                        @if($project->expenses)
                        @php ($i = 0)
                        @foreach($project->expenses as $expense)
                        <tr>
                            <td>{!! Form::text("expenses[$i][action]",$expense->action,['class'=>'form-control']) !!}</td>
                            <td>{!! Form::textarea("expenses[$i][details]",$expense->details,['class'=>'form-control']) !!}</td>
                            <td>{!! Form::text("expenses[$i][payee]",$expense->payee,['class'=>'form-control']) !!}</td>
                            <td dir="ltr">{!! Form::date("expenses[$i][date]",$expense->date,['class'=>'form-control']) !!}</td>
                            <td>{!! $expense->due_payment ? Form::checkbox("expenses[$i][due_payment]",$expense->due_payment,true) : Form::checkbox("expenses[$i][due_payment]",$expense->due_payment,false)  !!}</td>
                            <td>{!! Form::number("expenses[$i][cost]",$expense->cost,['class'=>'form-control']) !!}</td>
                            <td>{!! Form::number("expenses[$i][amount_paid]",$expense->amount_paid,['class'=>'form-control']) !!}</td>
                            <td><a href="{{url("/cp/invoice/$expense->invoice")}}">الفاتورة</a></td>
                            {!! Form::hidden("expenses[$i][id]",$expense->id) !!}
                        </tr>
                        @php ($i = $i + 1)
                        @endforeach
                        @else
                            Error
                        @endif
                        <tr>
                            <td colspan='7'><button class="btn btn-primary" id='addExpense' data-i="{{$i-1}}">أضف مصروف</button></td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="col-md-8 col-md-offset-2">
                {!! Form::submit('تعديل',['class'=>'btn btn-primary']) !!}
            </div>
             {!! Form::close() !!}
        </div>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            incomesI = $("#addIncome").data('i');
            $("#addIncome").click(function(){

                incomesI = incomesI + 1;

                $("#incomesTable tr:last").prev().after(
                    "<tr><td><input type='date' class='form-control' name='incomes["+incomesI+"][due_date]'/></td>\
                    <td><input type='date' class='form-control' name='incomes["+incomesI+"][received_date]'/></td>\
                    <td><input type='number' id='amount_due' class='form-control' name='incomes["+incomesI+"][amount_due]'/></td>\
                    <td><input type='number' id='amount_received' class='form-control' name='incomes["+incomesI+"][amount_received]'/></td>\
                    <td><textarea class='form-control' name='incomes["+incomesI+"][details]'/></td>\
                    <td><input type='file' name='incomes["+incomesI+"][invoice]'</td></tr>\
                    <input type='hidden' name='incomes["+incomesI+"][id]' value='new'/>"
                );

                return false;
            });
            expenseI = $("#addExpense").data('i');
            $("#addExpense").click(function(){
                expenseI = expenseI + 1;
                $("#expenseTable tr:last").prev().after(
                    "<tr><td><input type='text' class='form-control' name='expenses["+expenseI+"][action]'/></td>\
                    <td><textarea class='form-control' name='expenses["+expenseI+"][details]'></textarea></td>\
                    <td><input type='text' class='form-control' name='expenses["+expenseI+"][payee]'/></td>\
                    <td><input type='date' class='form-control' name='expenses["+expenseI+"][date]'</td>\
                    <td><input type='checkbox' name='expenses["+expenseI+"][due_payment]'/></td>\
                    <td><input type='number' class='form-control' name='expenses["+expenseI+"][cost]'/></td>\
                    <td><input type='number' class='form-control' name='expenses["+expenseI+"][amount_paid]'/></td></tr>\
                    <input type='hidden' name='expenses["+expenseI+"][id]' value='new'/>"
                )
                return false;
            });
            $('body').on('focus',".datepicker", function(){
                $(this).datepicker({
                    dateFormat: "yy-mm-dd"
                });
            });
        });
    </script>
@endsection
