@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">الرئيسية</div>

                <div class="panel-body">
                    <ul class="nav nav-tabs">
                        <li><a href='{{ url('/cp/projects') }}'>المشاريع</a></li>
                        <li><a href='{{ url('/cp/users')    }}'>المستخدمون</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
