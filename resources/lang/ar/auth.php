<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'هذا المدخل غير صحيح.',
    'throttle' => 'عدة محاولات فاشلة لتسجيل الدخول، رجاءً أعد المحاولة بعد بضعة ثوانٍ.',

];
