<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'يجب أن تكون كلمة المرور مكونة من 6 محارف على الأقل .',
    'reset' => 'تم تغيير كلمة مرورك!',
    'sent' => 'لقد قمنا بإرسال رابط تغيير كلمة المرور إلى بريدك الالكتروني!',
    'token' => 'تذكرة غير صالحة لتغيير كلمة المرور.',
    'user' => "لم نستطع إيجاد مستخدم بهذا البريد الالكتروني .",

];
