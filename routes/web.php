<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::group(['prefix'=>'cp','middleware'=>'auth'],function(){

    Route::get('/',['uses'=>'HomeController@index']);

    // Registration Routes...
    /*Route::get('users/create', 'Auth\RegisterController@showRegistrationForm')->name('register');
    Route::post('users/create', 'Auth\RegisterController@register');*/

    Route::resource('projects','ProjectsController');

    Route::resource('users','UsersController');

    Route::get('/invoices/income/{id}','InvoicesController@showIncomeInvoice');
    Route::get('/invoices/expense/{id}','InvoicesController@showExpenseInvoice');

});

Auth::routes();
