<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Incomes extends Model
{
    //
    protected $fillable = ['due_date','received_date','amount_due','amount_received','details','invoice'];
}
