<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Expenses extends Model
{
    //
    protected $fillable = ['action','details','payee','date','due_payment','cost','amount_paid','invoice'];
}
