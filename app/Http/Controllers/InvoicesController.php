<?php

namespace App\Http\Controllers;

use App;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class InvoicesController extends Controller
{
    //

    public function showIncomeInvoice($id){
        $id = (int)$id;
        $income = App\Incomes::find($id);
        $storagePath = storage_path('app\\'.$income['invoice']);
        return Image::make($storagePath)->response();
    }

    public function showExpenseInvoice($id){
        $id = (int)$id;
        $expense = App\Expenses::find($id);
        $storagePath = storage_path('app\\'.$expense['invoice']);
        return Image::make($storagePath)->response();
    }
}
