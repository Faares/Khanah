<?php

namespace App\Http\Controllers;

use App;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class ProjectsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        #$projects = DB::table('projects')->paginate(2);
        $projects = App\Projects::paginate(20);
        //
        return view('projects.index',['projects'=>$projects]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('projects.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            // project rules
            'project.name'=>'required|max:255',
            'project.details'=>'required',
            'project.payee'=>'required|max:255',

            // incomes rules
            'incomes.*.due_date'=>'required|date',
            'incomes.*.received_date'=>'required|date',
            'incomes.*.amount_due'=>'required|numeric',
            'incomes.*.amount_received'=>'required|numeric',
            'incomes.*.details'=>'required',

            // expenses rules
            'expenses.*.action'=>'required|max:255',
            'expenses.*.details'=>'required',
            'expenses.*.payee'=>'required|max:255',
            'expenses.*.date'=>'required|date',
            'expenses.*.due_payment'=>'boolean',
            'expenses.*.cost'=>'required|numeric',
            'expenses.*.amount_paid'=>'required|numeric'
        ],[
            'required'=>'هذا الحقل: :attribute ، مطلوب .'
        ]);

        if(isset($request->project) && !empty($request->project)){
            $project = App\Projects::create($request->project);
            if(isset($request->incomes) && !empty($request->incomes)){
                foreach($request->incomes as $key => $val){

                    $insert = $request->incomes[$key];

                    if(isset($request->incomes[$key]['invoice'])){
                        $invoice = $request->incomes[$key]['invoice'];
                        if(in_array(strtolower($invoice->getClientOriginalExtension()),['jpg','png','jpeg','gif'])){
                            $insert['invoice'] = str_replace('/','\\',$invoice->store('invoices/incomes'));
                        }
                    }

                    $project->incomes()->create($insert);
                }
            }
            if(isset($request->expenses) && !empty($request->expenses)){
                foreach($request->expenses as $key => $val){
                    $insert = $request->expenses[$key];
                    #return $request->all();
                    if(isset($request->expenses[$key]['invoice'])){
                        $invoice = $request->expenses[$key]['invoice'];
                        if(in_array(strtolower($invoice->getClientOriginalExtension()),['jpg','png','jpeg','gif'])){
                            $insert['invoice'] = str_replace('/','\\',$invoice->store('invoices/expenses'));
                        }
                    }

                    $project->expenses()->create($insert);
                }
            }


            return redirect()->route('projects.show',$project->id);
        }
        return false;
        //return redirect()->route('projects.show',$last_insert_id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $id = (int)$id;
        if($id != null){
            $project = App\Projects::find($id);
            return view('projects.show',['project'=>$project]);
        }else{
            return view('errors.404');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $id = (int)$id;
        if($id != null){
            $project = App\Projects::find($id);
            return view('projects.edit',['project'=>$project]);
        }else{
            return view('errors.404');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $id = (int)$id;
        if($id != null){
            $this->validate($request,[
                // project rules
                'project.name'=>'required|max:255',
                'project.details'=>'required',
                'project.payee'=>'required|max:255',

                // incomes rules
                'incomes.*.due_date'=>'required|date',
                'incomes.*.received_date'=>'required|date',
                'incomes.*.amount_due'=>'required|numeric',
                'incomes.*.amount_received'=>'required|numeric',
                'incomes.*.details'=>'required|alpha_dash',

                // expenses rules
                'expenses.*.action'=>'required|max:255',
                'expenses.*.details'=>'required',
                'expenses.*.payee'=>'required|max:255',
                'expenses.*.date'=>'required|date',
                'expenses.*.due_payment'=>'boolean',
                'expenses.*.cost'=>'required|numeric',
                'expenses.*.amount_paid'=>'required|numeric'
            ],[
                'required'=>'هذا الحقل: :attribute ، مطلوب .'
            ]);

            App\Projects::find($id)->update($request->project);

            if($request->incomes){
                foreach($request->incomes as $income){
                    if($income['id'] != 'new'){
                        App\Incomes::find($income['id'])->update($income);;
                    }else{
                        App\Incomes::create($income);
                    }
                }
            }

            if($request->expenses){
                foreach($request->expenses as $expense){
                    if($expense['id'] != 'new'){
                        App\Expenses::find($expense['id'])->update($expense);
                    }else{
                        App\Expenses::create($expense);
                    }
                }
            }
            $request->session()->flash('success','تم تحديث البيانات بنجاح.');

            return redirect()->route('projects.show',$id);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
