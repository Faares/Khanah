<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App;

class Projects extends Model
{
    protected $fillable = ['name','details','payee'];
    //
    public function incomes(){
        return $this->belongsToMany('App\Incomes');
    }

    public function expenses(){
        return $this->belongsToMany('App\Expenses');
    }

    public function updateExpense($id,$data){
        return App\Expenses::find($id)->update($data);
    }

    public function insertIncome($data){
        return App\Incomes::create($data);
    }

    public function insertExpense($data){
        return App\Expenses::create($data);
    }
}
