-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 29, 2017 at 11:25 PM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hamah`
--

-- --------------------------------------------------------

--
-- Table structure for table `expenses`
--

CREATE TABLE `expenses` (
  `id` int(10) UNSIGNED NOT NULL,
  `action` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `details` text COLLATE utf8_unicode_ci NOT NULL,
  `payee` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `due_payment` tinyint(1) NOT NULL,
  `cost` double NOT NULL,
  `amount_paid` double NOT NULL,
  `invoice` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `expenses`
--

INSERT INTO `expenses` (`id`, `action`, `details`, `payee`, `date`, `due_payment`, `cost`, `amount_paid`, `invoice`, `created_at`, `updated_at`) VALUES
(1, 'Hello Qorld', 'Hello', 'Project Y', '2017-01-01', 1, 15, 12, 2, NULL, '2017-01-19 13:26:46');

-- --------------------------------------------------------

--
-- Table structure for table `expenses_projects`
--

CREATE TABLE `expenses_projects` (
  `id` int(10) UNSIGNED NOT NULL,
  `expenses_id` int(11) NOT NULL,
  `projects_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `expenses_projects`
--

INSERT INTO `expenses_projects` (`id`, `expenses_id`, `projects_id`) VALUES
(1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `incomes`
--

CREATE TABLE `incomes` (
  `id` int(10) UNSIGNED NOT NULL,
  `due_date` date NOT NULL,
  `received_date` date NOT NULL,
  `amount_due` double NOT NULL,
  `amount_received` double NOT NULL,
  `details` text COLLATE utf8_unicode_ci NOT NULL,
  `invoice` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `incomes`
--

INSERT INTO `incomes` (`id`, `due_date`, `received_date`, `amount_due`, `amount_received`, `details`, `invoice`, `created_at`, `updated_at`) VALUES
(1, '2017-01-19', '2017-12-01', 100, 100, 'تجربة', 1, '2017-01-06 21:00:00', '2017-01-19 12:36:37'),
(2, '2017-01-07', '2017-01-07', 20170107, 20170107, 'تجربة2', 1, '2017-01-06 21:00:00', '2017-01-19 12:37:38'),
(3, '2016-01-01', '2017-01-01', 200, 200, 'تحربة', NULL, '2017-01-19 12:57:25', '2017-01-19 12:57:25');

-- --------------------------------------------------------

--
-- Table structure for table `incomes_projects`
--

CREATE TABLE `incomes_projects` (
  `id` int(10) UNSIGNED NOT NULL,
  `incomes_id` int(11) NOT NULL,
  `projects_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `incomes_projects`
--

INSERT INTO `incomes_projects` (`id`, `incomes_id`, `projects_id`) VALUES
(1, 1, 1),
(2, 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_12_29_223000_create_projects_table', 2),
(4, '2017_01_03_154149_create_incomes', 2),
(5, '2017_01_06_141649_incomes', 3),
(6, '2017_01_06_142528_incomes_projects', 4),
(7, '2017_01_07_000710_expenses', 5),
(8, '2017_01_07_001403_expenses_projects', 6);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('frs.ksa@gmail.com', 'fbf861f6eaaec01ffc2482233d14718b4ccbddffe712c361fbc1cfafbf0b27f4', '2017-01-02 17:20:56');

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE `projects` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `details` text COLLATE utf8_unicode_ci NOT NULL,
  `payee` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `projects`
--

INSERT INTO `projects` (`id`, `name`, `details`, `payee`, `created_at`, `updated_at`) VALUES
(1, 'عبدالعزيز كلب', 'تجربة، لملئ التجربة الحقيقية', 'أنا الجهة', '2017-01-03 21:00:00', '2017-01-19 15:02:10'),
(2, 'test1', 'test1', 'test1', '2017-01-04 21:00:00', '2017-01-04 21:00:00'),
(3, 'test', 'tes', 'test', '2017-01-03 21:00:00', '2017-01-03 21:00:00'),
(4, 'test1', 'test1', 'test', '2017-01-04 21:00:00', '2017-01-04 21:00:00'),
(5, 'test', 'test', 'test', '2017-01-03 21:00:00', '2017-01-03 21:00:00'),
(6, 'test1', 'test1', 'test1', '2017-01-04 21:00:00', '2017-01-04 21:00:00'),
(7, 'test', 'test', 'test', '2017-01-03 21:00:00', '2017-01-03 21:00:00'),
(8, 'test1', 'test1', 'test1', '2017-01-04 21:00:00', '2017-01-04 21:00:00'),
(9, 'test', 'test', 'test', '2017-01-03 21:00:00', '2017-01-03 21:00:00'),
(10, 'test1', 'test1', 'test1', '2017-01-04 21:00:00', '2017-01-04 21:00:00'),
(11, 'test', 'test', 'test', '2017-01-03 21:00:00', '2017-01-03 21:00:00'),
(12, 'test1', 'test1', 'test1', '2017-01-04 21:00:00', '2017-01-04 21:00:00'),
(13, 'test', 'test', 'test', '2017-01-03 21:00:00', '2017-01-03 21:00:00'),
(14, 'test1', 'test1', 'test1', '2017-01-04 21:00:00', '2017-01-04 21:00:00'),
(15, 'test', 'test', 'test', '2017-01-03 21:00:00', '2017-01-03 21:00:00'),
(16, 'test1', 'test1', 'test1', '2017-01-04 21:00:00', '2017-01-04 21:00:00'),
(17, 'test', 'test', 'test', '2017-01-03 21:00:00', '2017-01-03 21:00:00'),
(18, 'test1', 'test1', 'test1', '2017-01-04 21:00:00', '2017-01-04 21:00:00'),
(19, 'test', 'test', 'test', '2017-01-03 21:00:00', '2017-01-03 21:00:00'),
(20, 'test1', 'test1', 'test1', '2017-01-04 21:00:00', '2017-01-04 21:00:00'),
(21, 'test', 'test', 'test', '2017-01-03 21:00:00', '2017-01-03 21:00:00'),
(22, 'test1', 'test1', 'test1', '2017-01-04 21:00:00', '2017-01-04 21:00:00'),
(23, 'test', 'test', 'test', '2017-01-03 21:00:00', '2017-01-03 21:00:00'),
(24, 'test1', 'test1', 'test1', '2017-01-04 21:00:00', '2017-01-04 21:00:00'),
(25, 'test', 'test', 'test', '2017-01-03 21:00:00', '2017-01-03 21:00:00'),
(26, 'test1', 'test1', 'test1', '2017-01-04 21:00:00', '2017-01-04 21:00:00'),
(27, 'test', 'test', 'test', '2017-01-03 21:00:00', '2017-01-03 21:00:00'),
(28, 'test1', 'test1', 'test1', '2017-01-04 21:00:00', '2017-01-04 21:00:00'),
(29, 'test', 'test', 'test', '2017-01-03 21:00:00', '2017-01-03 21:00:00'),
(30, 'test1', 'test1', 'test1', '2017-01-04 21:00:00', '2017-01-04 21:00:00'),
(31, 'test', 'test', 'test', '2017-01-03 21:00:00', '2017-01-03 21:00:00'),
(32, 'test1', 'test1', 'test1', '2017-01-04 21:00:00', '2017-01-04 21:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `permission` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`, `permission`) VALUES
(1, 'Fares', 'frs.ksa@gmail.com', '$2y$10$vWLPMoiMXIzq079vUwpRNOapq29qAQQmVjjpwMuett9CciIUdwB7G', 'DUSKjriONbI1x7BjMhPDQmaCAsuV9vG7uNA7mdV8mXjQEZh9O8dbwWDRWtyN', '2016-12-29 19:17:54', '2017-01-09 14:30:16', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `expenses`
--
ALTER TABLE `expenses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `expenses_projects`
--
ALTER TABLE `expenses_projects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `incomes`
--
ALTER TABLE `incomes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `incomes_projects`
--
ALTER TABLE `incomes_projects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `expenses`
--
ALTER TABLE `expenses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `expenses_projects`
--
ALTER TABLE `expenses_projects`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `incomes`
--
ALTER TABLE `incomes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `incomes_projects`
--
ALTER TABLE `incomes_projects`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `projects`
--
ALTER TABLE `projects`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
